# DamID pipeline
Pipeline for DamID analysis.

## Setup
Clone the repository together with the submodules:
```
git clone --recurse-submodules https://gitlab.com/lapinskim/damid_pipeline.git
```

Install Nextflow from conda package manager:
```
conda install -c bioconda nextflow
```

Link or move FASTQ files to the main analysis folder(the directory name can be configured in `nextflow.config` file.):
```
cd damid_pipeline
ln -s /example/illumina_reads ./reads
```

Setup the working environment by creating the folder with the reference annotation (folder name can be configured in the `nextflow.config` file.).

Download current version of the reference genome.

Example commands:
```
mkdir ./ref
cd ref
wget ftp://ftp.ensembl.org/pub/release-100/fasta/danio_rerio/dna/Danio_rerio.GRCz11.dna_sm.primary_assembly.fa.gz
zcat Danio_rerio.GRCz11.dna_sm.primary_assembly.fa.gz >Danio_rerio.GRCz11.100.dna_sm.primary_assembly.fa
cd -
```

Configure the pipeline processing parameters by customizing the `nextflow.config` file.

Enter your samples into a new `design.csv` file by following the examples in the `design.csv.example` file. 

Run the pipeline with:
```
nextflow run -resume main.nf
```
