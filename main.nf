#!/usr/bin/env nextflow

/** Pipeline for the analysis of the DamID sequencing results
*/

// Input channels
if (params.table) {
    Channel
        .fromPath(params.table, checkIfExists: true)
        .set{ table_ch }
}

table_ch
    .splitCsv(header: true, sep:',')
    .map { row -> [
            row.sample_id,
            row.condition,
            row.factor,
            row.group,
            row.replicate,
            params.pair_end
            ? [
                file(params.fastq_dir + "/" + row.fastq1, checkIfExists: true),
                file(params.fastq_dir + "/" + row.fastq2, checkIfExists: true)
            ]
            : file(params.fastq_dir + "/" + row.fastq1, checkIfExists: true)
        ] }
    .into {
        paired_fastq_for_fastqc_ch;
        paired_fastq_for_cutadapt_ch
    }

Channel
    .fromPath("${params.genome_fasta}", checkIfExists: true)
    .into {
        reference_genome_ch1;
        reference_genome_ch2;
        reference_genome_ch3;
        reference_genome_ch4;
        reference_genome_ch5;
        reference_genome_ch6;
        reference_genome_ch7;
        reference_genome_ch8;
        reference_for_homer_ch;
        reference_for_homer_bg_ch;
        reference_for_annotate_ch
    }

Channel
    .fromPath("${params.annotation}", checkIfExists: true)
    .into {
        annotation_ch1;
        annotation_ch2;
        gtf_for_annotate_ch
    }

// Pipeline

process fastqc_initial {
    conda "bioconda::fastqc=0.11.9"

    cpus params.fastqc_cpus
    memory "20GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file(reads) from paired_fastq_for_fastqc_ch

    output:
    file "fastqc_initial/*.{zip,html}"

    """
    mkdir -v "./fastqc_initial"
    fastqc -o "./fastqc_initial" -t ${params.fastqc_cpus} -f fastq ${reads}
    """
}

process cutadapt {
    conda "bioconda::cutadapt=3.4 conda-forge::pigz=2.6"

    cpus "5"
    memory "20GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file(reads:"${sample_id}_R?.fastq.gz") from paired_fastq_for_cutadapt_ch

    output:
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file("cutadapt/*.trimmed.fastq.gz") into trimmed_fastq_for_fastqc_ch,
        trimmed_fastq_for_bwa_ch
    file "cutadapt/*.log" into cutadapt_log_ch

    script:
    paired_options = params.pair_end ?
        "-A ${params.cutadapt_adapter_r2} \
--pair-filter ${params.cutadapt_pair_filter} \
--paired-output ./cutadapt/${sample_id}_R2.trimmed.fastq.gz" :
        ""
    """
    mkdir -v "./cutadapt"
    cutadapt \
        --cores 5 \
        -a ${params.cutadapt_adapter_r1} \
        --error-rate ${params.cutadapt_error} \
        --quality-cutoff ${params.cutadapt_q} \
        --minimum-length ${params.cutadapt_length} \
        --output "./cutadapt/${sample_id}_R1.trimmed.fastq.gz" \
        ${paired_options} \
        ${reads} \
    | tee "cutadapt/${sample_id}.log"
    """
}

process fastqc {
    conda "bioconda::fastqc=0.11.9"

    cpus params.fastqc_cpus
    memory "20GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file(reads) from trimmed_fastq_for_fastqc_ch

    output:
    file "fastqc/*.{zip,html}" into fastqc_ch

    """
    mkdir -v "./fastqc"
    fastqc -o "./fastqc" -t ${params.fastqc_cpus} -f fastq ${reads}
    """
}

process bwa_index {
    conda "bioconda::bwa=0.7.17"

    cpus "1"
    memory "10GB"

    storeDir params.reference_dir

    input:
    file genome from reference_genome_ch1

    output:
    file "${index_name}.bwa.index/${index_name}*" into bwa_index_ch

    script:
    (full, species, genome_version, release_version) = (genome.baseName =~ /(.+)\.(.+)\.(\d+)\..*$/)[0]
    index_name = species + "." + genome_version + "." + release_version
    """
    mkdir -v "./${index_name}.bwa.index"
    bwa index -p "./${index_name}.bwa.index/${index_name}" ${genome}
    """
}

process fasta_index {
    conda "./envs/samtools.yaml"

    cpus "1"
    memory "5GB"

    storeDir params.reference_dir

    input:
    file genome from reference_genome_ch2

    output:
    file "${genome}.fai"
    file "${genome.getBaseName()}.genome" into chromosome_index_ch,
        genome_to_slop_ch,
        genome_to_intersect_for_fragments_ch,
        genome_for_windows_ch
    file "${genome.getBaseName()}.size" into genome_size_for_macs, genome_size_for_rev_macs

    """
    samtools faidx ${genome}
    cut -f 1,2 ${genome}.fai >${genome.getBaseName()}.genome
    awk '{len+=\$2} END {print len}' ${genome.getBaseName()}.genome \
      >${genome.getBaseName()}.size
    """
}

process extract_genes {
    cpus "1"
    memory "4GB"

    storeDir params.reference_dir

    input:
    file(annotation) from annotation_ch1

    output:
    file("${annotation_name}.genes.bed") into genes_ch

    script:
    annotation_name = (annotation.getName() =~ /^(.+)\.gtf.*$/)[0][1]
    """
    extract_features.py ${annotation} >${annotation_name}.genes.bed
    """
}

process restriction_sites {
    cpus "1"
    memory "2GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    file(genome_fasta) from reference_genome_ch3

    output:
    file("map_sequence/*.stats.txt")
    file("map_sequence/*.sites.bed") into restriction_sites_ch

    script:
    genome_name = (genome_fasta.getName() =~ /^(.+)\.fa.*$/)[0][1]
    """
    mkdir -v "./map_sequence"
    map_sequence.py \
        --verbose \
        --sequence GATC \
        --position 1 \
        ${genome_fasta} \
        1>./map_sequence/${genome_name}.sites.bed \
        2>./map_sequence/${genome_name}.stats.txt
    """
}

process restriction_fragments {
    cpus "1"
    memory "2GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    file(genome_fasta) from reference_genome_ch4

    output:
    file("map_sequence/*.fragments.bed") into fragments_to_intersect_ch
    file("map_sequence/*.fragments.bed") into fragments_to_intersect_ch2,
        fragments_to_intersect_reads

    script:
    genome_name = (genome_fasta.getName() =~ /^(.+)\.fa.*$/)[0][1]
    """
    mkdir -v "./map_sequence"
    map_sequence.py \
        --sequence GATC \
        --position 1 \
        --regions \
        ${genome_fasta} \
        1>./map_sequence/${genome_name}.fragments.bed
    """
}

process bwa_align {
    conda "./envs/bwa_align.yaml"

    cpus "16"
    memory "30GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    each file(index_files) from bwa_index_ch
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file(reads) from trimmed_fastq_for_bwa_ch

    output:
    tuple val(sample_id), file("bwa/${sample_id}.sorted.bam") into alignment_for_stats_ch, alignment_for_unmapped_ch
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file("bwa/${sample_id}.sorted.bam") into alignment_to_mark_dup_ch

    script:
    index_name = index_files[0].baseName
    """
    mkdir -v "./bwa"
    bwa mem \
        -M \
        -t 16 \
        ${index_name} \
        ${reads} \
        | samtools sort -O bam -o ./bwa/${sample_id}.sorted.bam
    """
}

process bamstats_gc {
    conda "./envs/samtools.yaml"

    cpus "1"
    memory "10GB"

    input:
    file genome_fasta from reference_genome_ch5

    output:
    file "${reference_name}.gc" into reference_gc_ch

    script:
    reference_name = genome_fasta.getName()
    """
    plot-bamstats -s ${genome_fasta} >"./${reference_name}.gc"
    """
}


process samtools_stats {
    conda "./envs/samtools.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(sorted_bam) from alignment_for_stats_ch
    each file(genome_fasta) from reference_genome_ch6

    output:
    file "samtools/${file_name}.bc" into samtools_stats_ch1, samtools_stats_ch2

    script:
    file_name = sorted_bam.getName()
    """
    mkdir -v "./samtools"
    samtools stats \
      -r ${genome_fasta} \
      ${sorted_bam} \
      >"./samtools/${file_name}.bc";
    """
}

process plot_bamstats {
    conda "./envs/plot_bamstats.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    file(stats_file) from samtools_stats_ch1
    each file(reference_gc) from reference_gc_ch

    output:
    file "bamstats/*"

    script:
    sample_id = stats_file.getSimpleName()
    """
    plot-bamstats \
      -p "bamstats/${sample_id}/${sample_id}" \
      -r ${reference_gc} \
      ${stats_file}
    """
}

process extract_unmapped {
    conda "./envs/samtools.yaml"

    cpus "1"
    memory "2GB"

    input:
    tuple val(sample_id), file(sorted_bam) from alignment_for_unmapped_ch

    output:
    tuple val(sample_id), file("*.fa") into unmapped_fasta_ch

    script:
    out_lines = params.n_blast.toInteger() * 2

    shell:
    '''
    samtools view -f 13 !{sorted_bam} \
    | samtools fasta -N - \
    | awk '{ if ($0 ~ /^>.*$/) { name=$0 } else { if ($0 !~ /N/) { OFS="\\n"; print name, $0 }}}' \
    | head -n !{out_lines} >!{sample_id}.unmapped.noN.fa
    '''
}

process scan_unmapped {
    conda "biopython=1.77 matplotlib=3.2.2"

    cpus "1"
    memory "2GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(unmapped_fasta) from unmapped_fasta_ch

    output:
    file "blast/*.{xml,pdf,tsv}" optional true

    """
    mkdir -v "./blast"
    blast_unmapped.py --identity ${params.blast_identity} ${unmapped_fasta} >${sample_id}.stats.tsv
    for f in ./*.{xml,pdf,tsv}
    do
        if [[ -s "\$f" ]]
        then
            mv -v "\$f" ./blast/
        fi
    done
    """
}

process mark_dup {
    conda "./envs/picard.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file(sorted_bam) from alignment_to_mark_dup_ch

    output:
    file("picard/${sample_id}.sorted.mkdup.bam") into alignment_to_table_ch
    tuple val(sample_id), file("picard/${sample_id}.sorted.mkdup.bam") into bam_for_index_ch,
        id_bam_for_summary_ch, bam_for_coverage_ch
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file("picard/${sample_id}.sorted.mkdup.bam") into alignment_for_preseq_counts_ch,
        alignment_to_filter_ch, alignment_to_index_ch
    file("picard/${sample_id}.dup_metrics.txt") into mark_dup_ch
    tuple val(sample_id),
        file("picard/${sample_id}.dup_metrics.txt") into stats_for_histogram_ch
    file("picard/${sample_id}.mark_dup.log")

    """
    mkdir -v "./picard"
    { picard \
        -Xmx10g \
        MarkDuplicates \
        -I ${sorted_bam} \
        -O ./picard/${sample_id}.sorted.mkdup.bam \
        -M ./picard/${sample_id}.dup_metrics.txt \
        --OPTICAL_DUPLICATE_PIXEL_DISTANCE ${params.markduplicates_distance}; } \
    &>./picard/${sample_id}.mark_dup.log
    """
}

process samtools_index {
    conda "./envs/samtools.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(sorted_bam) from bam_for_index_ch

    output:
    file("picard/*.bai") into bam_index_ch

    """
    mkdir -v "./picard"
    samtools index ${sorted_bam}
    mv -v *.bai "./picard/"
    """
}

process make_count_table {
    conda "./envs/samtools.yaml"

    cpus "48"
    memory "2GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    file(bams) from alignment_to_table_ch.collect()

    output:
    file("multiqc/count_table_mqc.txt") into count_table_ch
    file("multiqc/make_count_table.log")

    script:
    pe = params.pair_end ? "--pair-end" : ""
    """
    mkdir -v "./multiqc"
    make_count_table.py \
        --cpus ${task.cpus} \
        ${pe} \
        --included ${params.samtools_flag_include} \
        --excluded ${params.samtools_flag_exclude} \
        --quality ${params.samtools_min_qual} \
        ${bams} \
        >./multiqc/count_table_mqc.txt \
        2>./multiqc/make_count_table.log
    """
}

process extract_histogram {
    cpus "1"
    memory "2GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(dup_metrics) from stats_for_histogram_ch

    output:
    tuple val(sample_id),
        file("picard/${sample_id}.dup_hist.txt") into histogram_for_preseq_ch

    """
    mkdir -v ./picard
    extract_histogram.awk ${dup_metrics} >./picard/${sample_id}.dup_hist.txt
    """
}

process preseq {
    conda "./envs/preseq.yaml"

    cpus "1"
    memory "2GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(hist) from histogram_for_preseq_ch

    output:
    file("preseq/${sample_id}.future_yield.txt") into preseq_ch
    file("preseq/${sample_id}.log")

    """
    mkdir -v ./preseq
    { preseq lc_extrap \
        -verbose \
        -hist \
        -output preseq/${sample_id}.future_yield.txt \
        ${hist}; } \
    &>preseq/${sample_id}.log
    """
}

process preseq_counts {
    conda "./envs/samtools.yaml"

    cpus "2"
    memory "6GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file(mk_bam) from alignment_for_preseq_counts_ch
    output:
    file("preseq/${sample_id}.counts") into preseq_counts_ch

    """
    mkdir -v ./preseq
    echo \
"${sample_id}.future_yield.txt \
\$(samtools view -c -f${params.preseq_include_flag} -F${params.preseq_exclude_all_flag} -q${params.preseq_min_qual} ${mk_bam}) \
\$(samtools view -c -f${params.preseq_include_flag} -F${params.preseq_exclude_unique_flag} -q${params.preseq_min_qual} ${mk_bam})" \
    >./preseq/${sample_id}.counts
    """
}

process preseq_combine_counts {
    cpus "1"
    memory "2GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    file "preseq/*" from preseq_counts_ch.collect()

    output:
    file("preseq/preseq_real_counts.txt") into preseq_all_counts_ch

    """
    cat ./preseq/* >./preseq/preseq_real_counts.txt
    """
}

process samtools_filter {
    conda "./envs/samtools.yaml"

    cpus "1"
    memory "2GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file(mk_bam) from alignment_to_filter_ch

    output:
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file("picard/${out_name}"),
        file("picard/${out_name}.bai") into alignment_tosplit_ch,
        alignment_for_fragments_ch
    tuple val(sample_id),
        file("picard/${out_name}.count"),
        file("picard/${out_name}") into alignment_tobigwig_ch

    script:
    out_name = "${sample_id}.filtered.sorted.bam"
    """
    mkdir -v "./picard"
    samtools view \
        -b \
        -q${params.samtools_min_qual} \
        -f${params.samtools_flag_include} \
        -F${params.samtools_flag_exclude} \
        ${mk_bam} >./picard/${out_name}
    samtools index ./picard/${out_name} ./picard/${out_name}.bai
    samtools view -c ./picard/${out_name} >./picard/${out_name}.count
    """
}

alignment_tosplit_ch
    .branch {
        control: it[3] == "control"
        treatment: it[3] == "treatment"
    }
    .set { samples_branched_ch }

samples_branched_ch.control
    .cross(samples_branched_ch.treatment) { it -> it[1, 4] }
    .map {
        it ->
        def control_set = it[0]
        def treatment_set = it[1]
        def sample_id = treatment_set[0]
        def condition = treatment_set[1]
        def factor = treatment_set[2]
        def treatment_bam = treatment_set[5]
        def treatment_index = treatment_set[6]
        def control_bam = control_set[5]
        def control_index = control_set[6]
        [ sample_id, condition, factor, treatment_bam, treatment_index, control_bam, control_index ]
    }
    .into {
        treatment_control_bam_for_macs2_ch;
        treatment_control_bam_for_fragmentsize_ch;
        treatment_control_bam_for_fingerprint_ch;
        treatment_control_for_rev_macs_ch
    }

process macs2 {
    conda "./envs/macs2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        file(treatment_bam),
        file(treatment_index),
        file(control_bam),
        file(control_index) from treatment_control_bam_for_macs2_ch
    each file(genome_size) from genome_size_for_macs

    output:
    file "macs2/${sample_id}/*"
    tuple val(sample_id), file("macs2/${sample_id}/*.narrowPeak") into macs2_peaks_ch
    file "macs2/${sample_id}/${sample_id}_peaks.xls" into macs2_qc_ch
    tuple val(sample_id),
        val(condition),
        val(factor),
        file("macs2/${sample_id}/${sample_id}_treat_pileup.bdg"),
        file("macs2/${sample_id}/${sample_id}_control_lambda.bdg") into bdgs_for_pvalue_ch

    """
    mkdir -vp "./macs2/${sample_id}"
    macs2 callpeak \
        --treatment ${treatment_bam} \
        --control ${control_bam} \
        --name ${sample_id} \
        --outdir "./macs2/${sample_id}" \
        --format ${params.macs2_format} \
        --gsize "\$(cat ${genome_size})" \
        --qvalue ${params.macs2_qvalue} \
        --keep-dup ${params.macs2_keep_dup} \
        --bdg \
        ${params.macs2_additional} \
        2>&1 \
        | tee "./macs2/${sample_id}/${sample_id}_macs2.log"
    """
}

process macs2_pvalue {
    conda "./envs/macs2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        file(treat_pileup),
        file(control_lambda) from bdgs_for_pvalue_ch

    output:
    tuple val(condition),
        val(factor),
        file("macs2/${sample_id}/${sample_id}_pvalue.bdg") into pvalue_bdg_ch

    when:
    params.cmbreps

    """
    mkdir -vp "./macs2/${sample_id}"
    macs2 bdgcmp \
        --tfile ${treat_pileup} \
        --cfile ${control_lambda} \
        --method ppois \
        --ofile "./macs2/${sample_id}/${sample_id}_pvalue.bdg" \
    2>&1 \
    | tee "./macs2/${sample_id}/${sample_id}_macs2_pvalue.log"
    """
}

pvalue_bdg_ch
    .groupTuple(by: [0, 1])
    .set { pvalue_grouped_ch }

process macs2_cmbreps {
    conda "./envs/macs2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(condition),
        val(factor),
        file(replicates) from pvalue_grouped_ch

    output:
    tuple val(condition),
        val(factor),
        file("${out_dir}/${prefix}_combined_pvalue.bdg") into combined_pvalue_ch

    when:
    params.cmbreps

    script:
    prefix = "${condition}_${factor}"
    out_dir = "macs2/${prefix}"
    """
    mkdir -vp "./${out_dir}"
    macs2 cmbreps \
        -i ${replicates} \
        -m fisher \
        --outdir "./${out_dir}" \
        --ofile ${prefix}_combined_pvalue.bdg \
    2>&1 \
    | tee "./${out_dir}/${prefix}_cmbreps.log"
    """
}

process macs2_p2q {
    conda "./envs/macs2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(condition),
        val(factor),
        file(pvalue) from combined_pvalue_ch

    output:
    tuple val(condition),
        val(factor),
        file("${out_dir}/${prefix}_combined_qvalue.bdg") into combined_qvalue_ch

    when:
    params.cmbreps

    script:
    prefix = "${condition}_${factor}"
    out_dir = "macs2/${prefix}"
    """
    mkdir -vp "./${out_dir}"
    macs2 bdgopt \
        -i ${pvalue} \
        -m p2q \
        --outdir "./${out_dir}" \
        --ofile ${prefix}_combined_qvalue.bdg \
    2>&1 \
    | tee "./${out_dir}/${prefix}_p2q.log"
    """
}

process macs2_bdgpeakcall {
    conda "./envs/macs2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(condition),
        val(factor),
        file(qvalue) from combined_qvalue_ch

    output:
    file("${out_dir}/*")
    tuple val(prefix),
        file("${out_dir}/${prefix}_combined.narrowpeak") into macs2_combined_peaks_ch,
        combined_peaks_to_annotate_ch

    when:
    params.cmbreps

    script:
    prefix = condition + "_" + factor
    out_dir = "macs2/${prefix}"
    """
    mkdir -vp "./${out_dir}"
    macs2 bdgpeakcall \
        -i ${qvalue} \
        --cutoff ${params.macs2_qvalue_cutoff} \
        --min-length ${params.fragment_size} \
        --max-gap ${params.read_length} \
        --outdir "./${out_dir}" \
        --ofile ${prefix}_combined.narrowpeak \
    2>&1 \
    | tee "./${out_dir}/${prefix}_bdgpeakcall.log"
    """
}

process bamPEFragmentSize {
    conda "./envs/deeptools.yaml"

    cpus "4"
    memory "5GB"

    publishDir params.out_dir, mode: "rellink"

    when:
    params.pair_end

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        file(treatment_bam),
        file(treatment_index),
        file(control_bam),
        file(control_index) from treatment_control_bam_for_fragmentsize_ch

    output:
    file "$out_dir/*"
    tuple file("$out_dir/${sample_id}.fragment.tsv"), file("$out_dir/${sample_id}.fragment_lengths.tsv") into fragment_qc_ch

    script:
    out_dir = "bamPEFragmentSize/$sample_id"
    """
    mkdir -vp "${out_dir}"
    bamPEFragmentSize \
        --bamfiles ${treatment_bam} ${control_bam} \
        --histogram ${out_dir}/${sample_id}.fragment.pdf \
        --numberOfProcessors 4 \
        --plotTitle "Fragment size of ${sample_id} and control" \
        --maxFragmentLength 1000 \
        --distanceBetweenBins 10000 \
        --table ${out_dir}/${sample_id}.fragment.tsv \
         --outRawFragmentLengths ${out_dir}/${sample_id}.fragment_lengths.tsv \
        | tee ${out_dir}/${sample_id}.fragment.out
    """
}

process plotFingerprint {
    conda "./envs/deeptools.yaml"

    cpus "4"
    memory "5GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        file(treatment_bam),
        file(treatment_index),
        file(control_bam),
        file(control_index) from treatment_control_bam_for_fingerprint_ch

    output:
    file "$out_dir/*"
    tuple file("${out_dir}/${sample_id}.qcmetrics.txt"), file("${out_dir}/${sample_id}.fingerprint_counts.tsv") into fingerprint_qc_ch

    script:
    out_dir = "plotFingerprint/$sample_id"
    """
    mkdir -vp "${out_dir}"
    plotFingerprint \
        --bamfiles ${treatment_bam} ${control_bam} \
        --plotFile ${out_dir}/${sample_id}.fingerprint.pdf \
        --ignoreDuplicates \
        --binSize 500 \
        --numberOfSamples 1000000 \
        --plotTitle "Fingerprint of ${sample_id} and control" \
        --outQualityMetrics ${out_dir}/${sample_id}.qcmetrics.txt \
        --outRawCounts ${out_dir}/${sample_id}.fingerprint_counts.tsv \
        --JSDsample ${control_bam} \
        --numberOfProcessors 4
    """
}

process make_bigwigs {
    conda "./envs/make_bigwigs.yaml"

    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        file(counts),
        file(sorted_bam) from alignment_tobigwig_ch
    each file(chrom_sizes) from chromosome_index_ch

    output:
    tuple val(sample_id), file("bigwig/*.bw") into bigwig_ch1,
        bigwig_ch2,
        bigwig_for_fragments_ch,
        bigwig_for_sites_ch
    file "bigwig/*.scale_factor.txt"

    """
    mkdir -v "./bigwig"
    SCALE_FACTOR=\$(echo "1000000 / `cat ${counts}`" | bc -l)
    echo \$SCALE_FACTOR >./bigwig/${sample_id}.scale_factor.txt
    bedtools genomecov \
        -ibam ${sorted_bam} \
        -bg \
        -scale \$SCALE_FACTOR \
        ${params.bedtools_genomecov_fragments} \
        | sort -T "./" -k1,1 -k2,2n \
        >${sample_id}.bedGraph

    bedGraphToBigWig ${sample_id}.bedGraph ${chrom_sizes} ./bigwig/${sample_id}.bw
    """
}

process plotProfile_gene {
    conda "./envs/deeptools.yaml"

    cpus "4"
    memory "5GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(bigwig) from bigwig_ch1
    each file(genes_bed) from genes_ch

    output:
    file "${out_dir}/*"

    script:
    out_dir = "plotProfile_gene/${sample_id}"
    """
    mkdir -vp "./${out_dir}"
    computeMatrix scale-regions \
        --regionsFileName ${genes_bed} \
        --scoreFileName ${bigwig} \
        --outFileName ${sample_id}.matrix.gz \
        --regionBodyLength 2000 \
        --beforeRegionStartLength 1000 \
        --afterRegionStartLength 1000 \
        --missingDataAsZero \
        --skipZeros \
        --smartLabels \
        --numberOfProcessors 4
    plotProfile \
        --matrixFile ${sample_id}.matrix.gz \
        --outFileName ${out_dir}/${sample_id}.profile.pdf \
        --outFileNameData ${out_dir}/${sample_id}.profile.tsv
    plotHeatmap \
        --colorMap BuPu \
        --legendLocation "none" \
        --matrixFile ${sample_id}.matrix.gz \
        --outFileName ${out_dir}/${sample_id}.heatmap.pdf
    """
}

process plotProfile_metagene {
    conda "./envs/deeptools.yaml"

    cpus "4"
    memory "5GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(bigwig) from bigwig_ch2
    each file(annotation_gtf) from annotation_ch2

    output:
    file "${out_dir}/*"

    script:
    out_dir = "plotProfile_metagene/${sample_id}"
    """
    mkdir -vp "./${out_dir}"
    computeMatrix scale-regions \
        --regionsFileName ${annotation_gtf} \
        --metagene \
        --scoreFileName ${bigwig} \
        --outFileName ${sample_id}.matrix.gz \
        --regionBodyLength 2000 \
        --beforeRegionStartLength 1000 \
        --afterRegionStartLength 1000 \
        --missingDataAsZero \
        --skipZeros \
        --smartLabels \
        --numberOfProcessors 4
    plotProfile \
        --matrixFile ${sample_id}.matrix.gz \
        --outFileName ${out_dir}/${sample_id}.profile.pdf \
        --outFileNameData ${out_dir}/${sample_id}.profile.tsv
    plotHeatmap \
        --colorMap BuPu \
        --legendLocation "none" \
        --matrixFile ${sample_id}.matrix.gz \
        --outFileName ${out_dir}/${sample_id}.heatmap.pdf
    """
}

process filter_peaks {
    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(peaks) from macs2_peaks_ch

    output:
    tuple val(sample_id),
        file("${out_dir}/${sample_id}_filtered.narrowPeak") into filtered_peaks_ch,
        filtered_peaks_to_annotate_ch

    script:
    out_dir = "macs2/${sample_id}"

    shell:
    '''
    mkdir -vp "./!{out_dir}"
    awk '{ if ($9 >= -log(!{params.macs2_qvalue_filter})/log(10)) {print $0} }' !{peaks} \
        >"./!{out_dir}/!{sample_id}_filtered.narrowPeak"
    '''
}

peaks_to_intersect_ch =  params.cmbreps ? macs2_combined_peaks_ch : filtered_peaks_ch
peaks_to_annotate_ch = params.cmbreps ? combined_peaks_to_annotate_ch : filtered_peaks_to_annotate_ch

process annotate_peaks {
    conda "./envs/homer.yaml"

    cpus "6"
    memory "8GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(peaks) from peaks_to_annotate_ch
    each file(genome_fasta) from reference_for_annotate_ch
    each file(gtf) from gtf_for_annotate_ch

    output:
    file "${out_dir}/${sample_id}_annotatePeaks.tsv"

    script:
    out_dir = "macs2/${sample_id}"

    """
    mkdir -vp "./${out_dir}"
    annotatePeaks.pl \
        ${peaks} \
        ${genome_fasta} \
        -gtf ${gtf} \
        -gid \
        -cpu 6 \
        > "./${out_dir}/${sample_id}_annotatePeaks.tsv"
    """
}

process intersect_fragments {
    conda "bioconda::bedtools=2.29.2"

    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(peaks) from peaks_to_intersect_ch
    each file(fragments_bed) from fragments_to_intersect_ch

    output:
    tuple val(sample_id), file("intersect/*.bed") into fragments_with_peaks_ch

    """
    mkdir -v "./intersect"
    sort -k1,1 -k2,2n ${fragments_bed} \
        | bedtools intersect \
            -a - \
            -b ${peaks} \
            -f ${params.bedtools_intersect_fraction} \
            -u \
            -sorted \
            >./intersect/${sample_id}_fragments_peaks.bed
    """
}

process intersect_fragments_for_profile {
    conda "bioconda::bedtools=2.29.2"

    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    each file(fragments_bed) from fragments_to_intersect_reads
    each file(genome) from genome_to_intersect_for_fragments_ch
    tuple val(sample_id),
        val(condition),
        val(factor),
        val(group),
        val(replicate),
        file(bam_file),
        file(index_file) from alignment_for_fragments_ch

    output:
    tuple val(sample_id), file("intersect/${sample_id}_fragments.bed") into fragments_to_cross_ch,
        fragments_for_sites_ch

    """
    mkdir -v "./intersect"
    bedtools intersect \
        -a ${fragments_bed} \
        -b ${bam_file} \
        -u \
        -g ${genome} \
        -sorted \
        >./intersect/${sample_id}_fragments.bed
    """
}

bigwig_for_fragments_ch
    .cross(fragments_to_cross_ch)
    .map {
        it ->
        def sample_id = it[0][0]
        def bigwig = it[0][1]
        def fragments = it[1][1]
        [ sample_id, bigwig, fragments ]
    }
    .set { fragments_to_profile_ch }

process plotProfile_fragments {
    conda "./envs/deeptools.yaml"

    cpus "4"
    memory "5GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        file(bigwig),
        file(fragments_bed) from fragments_to_profile_ch

    output:
    file "${out_dir}/*"
    file "${out_dir}/${sample_id}.profile.tsv" into profile_qc_ch

    script:
    out_dir = "plotProfile_fragments/${sample_id}"
    """
    mkdir -vp "./${out_dir}"
    computeMatrix scale-regions \
        --regionsFileName ${fragments_bed} \
        --scoreFileName ${bigwig} \
        --outFileName ${sample_id}.matrix.gz \
        --regionBodyLength 500 \
        --beforeRegionStartLength 500 \
        --afterRegionStartLength 500 \
        --missingDataAsZero \
        --skipZeros \
        --smartLabels \
        --numberOfProcessors 4
    plotProfile \
        --startLabel ${params.deeptools_label} \
        --endLabel cut_2 \
        --regionsLabel "regions" \
        --matrixFile ${sample_id}.matrix.gz \
        --outFileName ${out_dir}/${sample_id}.profile.pdf \
        --outFileNameData ${out_dir}/${sample_id}.profile.tsv
    plotHeatmap \
        --startLabel cut_1 \
        --endLabel cut_2 \
        --regionsLabel "regions" \
        --legendLocation "none" \
        --colorMap BuPu \
        --matrixFile ${sample_id}.matrix.gz \
        --outFileName ${out_dir}/${sample_id}.heatmap.pdf
    """
}

process intersect_sites {
    conda "bioconda::bedtools=2.29.2"

    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(fragments_bed) from fragments_for_sites_ch
    each file(genome) from genome_to_slop_ch
    each file(sites_bed) from restriction_sites_ch

    output:
    tuple val(sample_id), file("intersect/${sample_id}_sites.bed") into sites_to_cross_ch

    """
    mkdir -v "./intersect"
    bedtools slop \
        -i ${fragments_bed} \
        -g ${genome} \
        -l 1 \
        -r 0 \
        | bedtools intersect \
            -a ${sites_bed} \
            -b - \
            -u \
            -sorted \
            >./intersect/${sample_id}_sites.bed
    """
}

bigwig_for_sites_ch
    .cross(sites_to_cross_ch)
    .map {
        it ->
            def sample_id = it[0][0]
            def bigwig = it[0][1]
            def sites = it[1][1]
            [ sample_id, bigwig, sites ]
    }
    .set { sites_to_profile_ch }


process plotProfile_sites {
    conda "./envs/deeptools.yaml"

    cpus "4"
    memory "5GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(bigwig), file(sites_bed) from sites_to_profile_ch

    output:
    file "${out_dir}/*"

    script:
    out_dir = "plotProfile_sites/${sample_id}"
    """
    mkdir -vp "./${out_dir}"
    computeMatrix reference-point \
        --regionsFileName ${sites_bed} \
        --scoreFileName ${bigwig} \
        --outFileName ${sample_id}.matrix.gz \
        --beforeRegionStartLength 250 \
        --afterRegionStartLength 250 \
        --missingDataAsZero \
        --skipZeros \
        --smartLabels \
        --numberOfProcessors 4
    plotProfile \
        --matrixFile ${sample_id}.matrix.gz \
        --refPointLabel cut \
        --regionsLabel "cut sites" \
        --outFileName ${out_dir}/${sample_id}.profile.pdf \
        --outFileNameData ${out_dir}/${sample_id}.profile.tsv
    plotHeatmap \
        --colorMap BuPu \
        --refPointLabel cut \
        --regionsLabel "cut sites" \
        --legendLocation "none" \
        --matrixFile ${sample_id}.matrix.gz \
        --outFileName ${out_dir}/${sample_id}.heatmap.pdf
    """
}

process discard_fragments {
    conda "bioconda::bedtools=2.29.2"

    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(fragments) from fragments_with_peaks_ch

    output:
    tuple val(sample_id), file("intersect/*.kept.bed") into kept_fragments_ch,
        kept_for_homer_ch,
        kept_for_homer_builtin_ch,
        kept_for_homer_bg_ch

    script:
    out_name = fragments.getSimpleName() + ".kept.bed"

    shell:
    '''
    mkdir -v "./intersect"
    awk '{ if ($3 - $2 >= !{params.meme_minw}) {print $0} }' !{fragments} \
        >./intersect/!{out_name}
    '''
}

process getfasta {
    conda "bioconda::bedtools=2.29.2"

    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(fragments) from kept_fragments_ch
    each file(genome_fasta) from reference_genome_ch7

    output:
    tuple val(sample_id), file("getfasta/${sample_id}.fragments.fa") into peaks_fasta_ch, peaks_fasta_ch2

    """
    mkdir -v "./getfasta"
    bedtools getfasta \
        -fi ${genome_fasta} \
        -bed ${fragments} \
        >./getfasta/${sample_id}.fragments.fa
    """
}

process meme {
    conda "./envs/meme.yaml"

    cpus "8"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(peaks_fasta) from peaks_fasta_ch

    output:
    file "${out_dir}/*"

    script:
    out_dir = "meme/${sample_id}"
    """
    mkdir -vp "./${out_dir}"
    meme \
        ${peaks_fasta} \
        -oc ${out_dir} \
        -objfun classic \
        -dna \
        -revcomp \
        -mod zoops \
        -nmotifs 50 \
        -evt ${params.meme_evt} \
        -minw ${params.meme_minw} \
        -maxw ${params.meme_maxw} \
        -markov_order ${params.meme_markov_order} \
        -p 8 \
        2>&1 \
        | tee ${out_dir}/${sample_id}.log
    """
}

process homer {
    conda "./envs/homer.yaml"

    cpus "8"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(fragments) from kept_for_homer_ch
    each file(genome_fasta) from reference_for_homer_ch

    output:
    file "${out_dir}/*"

    script:
    out_dir = "homer/${sample_id}"
    """
    mkdir -vp "./${out_dir}"
    findMotifsGenome.pl \
        ${fragments} \
        ${genome_fasta} \
        ${out_dir} \
        -h \
        -size given \
        -len ${params.homer_len} \
        -p 8 \
        -S 25 \
        -gc \
        2>&1 \
        | tee ${out_dir}/${sample_id}.log
    """
}

process homer_builtin {
    conda "./envs/homer.yaml"

    cpus "8"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(fragments) from kept_for_homer_builtin_ch

    output:
    file "${out_dir}/*"

    script:
    out_dir = "homer_builtin/${sample_id}"
    """
    mkdir -vp "./${out_dir}"
    findMotifsGenome.pl \
        ${fragments} \
        danRer11 \
        ${out_dir} \
        -size given \
        -len ${params.homer_len} \
        -p 8 \
        -S 25 \
        -gc \
        2>&1 \
        | tee ${out_dir}/${sample_id}.log
    """
}

process rev_macs2 {
    conda "./envs/macs2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        file(treatment_bam),
        file(treatment_index),
        file(control_bam),
        file(control_index) from treatment_control_for_rev_macs_ch
    each file(genome_size) from genome_size_for_rev_macs

    output:
    file "macs2/bg_${sample_id}/*"
    tuple val(sample_id), file("macs2/bg_${sample_id}/bg_*.narrowPeak") into background_peaks_ch
    tuple val(sample_id),
        val(condition),
        val(factor),
        file("macs2/bg_${sample_id}/bg_${sample_id}_treat_pileup.bdg"),
        file("macs2/bg_${sample_id}/bg_${sample_id}_control_lambda.bdg") into bg_bdgs_for_pvalue_ch

    """
    mkdir -vp "./macs2/bg_${sample_id}"
    macs2 callpeak \
        --treatment ${control_bam} \
        --control ${treatment_bam} \
        --name bg_${sample_id} \
        --outdir "./macs2/bg_${sample_id}" \
        --format ${params.macs2_format} \
        --gsize "\$(cat ${genome_size})" \
        --qvalue ${params.macs2_qvalue} \
        --keep-dup ${params.macs2_keep_dup} \
        --bdg \
        ${params.macs2_additional} \
        2>&1 \
        | tee "./macs2/bg_${sample_id}/bg_${sample_id}_macs2.log"
    """
}

process bg_macs2_pvalue {
    conda "./envs/macs2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id),
        val(condition),
        val(factor),
        file(treat_pileup),
        file(control_lambda) from bg_bdgs_for_pvalue_ch

    output:
    tuple val(condition),
        val(factor),
        file("macs2/bg_${sample_id}/bg_${sample_id}_pvalue.bdg") into bg_pvalue_bdg_ch

    when:
    params.cmbreps

    """
    mkdir -vp "./macs2/bg_${sample_id}"
    macs2 bdgcmp \
        --tfile ${treat_pileup} \
        --cfile ${control_lambda} \
        --method ppois \
        --ofile "./macs2/bg_${sample_id}/bg_${sample_id}_pvalue.bdg" \
    2>&1 \
    | tee "./macs2/bg_${sample_id}/bg_${sample_id}_macs2_pvalue.log"
    """
}

bg_pvalue_bdg_ch
    .groupTuple(by: [0, 1])
    .set { bg_pvalue_grouped_ch }

process bg_macs2_cmbreps {
    conda "./envs/macs2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(condition),
        val(factor),
        file(replicates) from bg_pvalue_grouped_ch

    output:
    tuple val(condition),
        val(factor),
        file("${out_dir}/bg_${prefix}_combined_pvalue.bdg") into bg_combined_pvalue_ch

    when:
    params.cmbreps

    script:
    prefix = "${condition}_${factor}"
    out_dir = "macs2/bg_${prefix}"
    """
    mkdir -vp "./${out_dir}"
    macs2 cmbreps \
        -i ${replicates} \
        -m fisher \
        --outdir "./${out_dir}" \
        --ofile bg_${prefix}_combined_pvalue.bdg \
    2>&1 \
    | tee "./${out_dir}/bg_${prefix}_cmbreps.log"
    """
}

process bg_macs2_p2q {
    conda "./envs/macs2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(condition),
        val(factor),
        file(pvalue) from bg_combined_pvalue_ch

    output:
    tuple val(condition),
        val(factor),
        file("${out_dir}/bg_${prefix}_combined_qvalue.bdg") into bg_combined_qvalue_ch

    when:
    params.cmbreps

    script:
    prefix = "${condition}_${factor}"
    out_dir = "macs2/bg_${prefix}"
    """
    mkdir -vp "./${out_dir}"
    macs2 bdgopt \
        -i ${pvalue} \
        -m p2q \
        --outdir "./${out_dir}" \
        --ofile bg_${prefix}_combined_qvalue.bdg \
    2>&1 \
    | tee "./${out_dir}/bg_${prefix}_p2q.log"
    """
}

process bg_macs2_bdgpeakcall {
    conda "./envs/macs2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(condition),
        val(factor),
        file(qvalue) from bg_combined_qvalue_ch

    output:
    file("${out_dir}/*")
    tuple val(prefix),
        file("${out_dir}/bg_${prefix}_combined.narrowpeak") into bg_macs2_combined_peaks_ch

    when:
    params.cmbreps

    script:
    prefix = condition + "_" + factor
    out_dir = "macs2/bg_${prefix}"
    """
    mkdir -vp "./${out_dir}"
    macs2 bdgpeakcall \
        -i ${qvalue} \
        --cutoff ${params.macs2_qvalue_cutoff} \
        --min-length ${params.fragment_size} \
        --max-gap ${params.read_length} \
        --outdir "./${out_dir}" \
        --ofile bg_${prefix}_combined.narrowpeak \
    2>&1 \
    | tee "./${out_dir}/bg_${prefix}_bdgpeakcall.log"
    """
}

process filter_background_peaks {
    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(peaks) from background_peaks_ch

    output:
    tuple val(sample_id), file("${out_dir}/bg_${sample_id}_filtered.narrowPeak") into filtered_background_peaks_ch

    script:
    out_dir = "macs2/bg_${sample_id}"

    shell:
    '''
    mkdir -vp "./!{out_dir}"
    awk '{ if ($9 >= -log(!{params.macs2_qvalue_filter})/log(10)) {print $0} }' !{peaks} \
        >"./!{out_dir}/bg_!{sample_id}_filtered.narrowPeak"
    '''
}

background_peaks_ch = params.cmbreps ? bg_macs2_combined_peaks_ch : filtered_background_peaks_ch

process intersect_bg_fragments {
    conda "bioconda::bedtools=2.29.2"

    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(peaks) from background_peaks_ch
    each file(fragments_bed) from fragments_to_intersect_ch2

    output:
    tuple val(sample_id), file("intersect/bg_*.bed") into fragments_with_background_peaks_ch

    """
    mkdir -v "./intersect"
    sort -k1,1 -k2,2n ${fragments_bed} \
        | bedtools intersect \
            -a - \
            -b ${peaks} \
            -f ${params.bedtools_intersect_fraction} \
            -u \
            -sorted \
            >./intersect/bg_${sample_id}_fragments_peaks.bed
    """
}

process discard_bg_fragments {
    conda "bioconda::bedtools=2.29.2"

    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(fragments) from fragments_with_background_peaks_ch

    output:
    tuple val(sample_id), file("intersect/*.kept.bed") into kept_background_fragments_ch, kept_background_for_homer_ch

    script:
    out_name = fragments.getSimpleName() + ".kept.bed"

    shell:
    '''
    mkdir -v "./intersect"
    awk '{ if ($3 - $2 >= !{params.meme_minw}) {print $0} }' !{fragments} \
        >./intersect/!{out_name}
    '''
}

process getfasta_bg {
    conda "bioconda::bedtools=2.29.2"

    cpus "1"
    memory "4GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(fragments) from kept_background_fragments_ch
    each file(genome_fasta) from reference_genome_ch8

    output:
    tuple val(sample_id), file("getfasta/bg_${sample_id}.fragments.fa") into background_peaks_fasta_ch

    """
    mkdir -v "./getfasta"
    bedtools getfasta \
        -fi ${genome_fasta} \
        -bed ${fragments} \
        >./getfasta/bg_${sample_id}.fragments.fa
    """
}

peaks_fasta_ch2
    .cross(background_peaks_fasta_ch)
    .map {
        it ->
            def sample_id = it[0][0]
            def peaks_fasta = it[0][1]
            def background_fasta = it[1][1]
            [ sample_id, peaks_fasta, background_fasta ]
    }
    .into { peaks_with_background_fasta_ch1;
            peaks_with_background_fasta_ch2
    }

process meme_de {
    conda "./envs/meme.yaml"

    cpus "8"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(peaks_fasta), file(background_fasta) from peaks_with_background_fasta_ch1

    output:
    file "${out_dir}/*"

    script:
    out_dir = "meme_de/${sample_id}"
    """
    mkdir -vp "./${out_dir}"
    meme \
        ${peaks_fasta} \
        -oc ${out_dir} \
        -neg ${background_fasta} \
        -objfun de \
        -dna \
        -revcomp \
        -mod zoops \
        -nmotifs 50 \
        -evt ${params.meme_evt} \
        -minw ${params.meme_minw} \
        -maxw ${params.meme_maxw} \
        -markov_order ${params.meme_markov_order} \
        -p 8 \
        2>&1 \
        | tee ${out_dir}/${sample_id}.log
    """
}

process meme_se {
    conda "./envs/meme.yaml"

    cpus "8"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(peaks_fasta), file(background_fasta) from peaks_with_background_fasta_ch2

    output:
    file "${out_dir}/*"

    script:
    out_dir = "meme_se/${sample_id}"
    """
    mkdir -vp "./${out_dir}"
    meme \
        ${peaks_fasta} \
        -oc ${out_dir} \
        -neg ${background_fasta} \
        -objfun se \
        -dna \
        -revcomp \
        -mod zoops \
        -nmotifs 50 \
        -evt ${params.meme_evt} \
        -minw ${params.meme_minw} \
        -maxw ${params.meme_maxw} \
        -markov_order ${params.meme_markov_order} \
        -p 8 \
        2>&1 \
        | tee ${out_dir}/${sample_id}.log
    """
}

kept_for_homer_bg_ch
    .cross(kept_background_for_homer_ch)
    .map {
        it ->
            def sample_id = it[0][0]
            def kept_peaks = it[0][1]
            def kept_background = it[1][1]
            [ sample_id, kept_peaks, kept_background ]
    }
    .set { peaks_with_background_homer_ch }

process homer_bg {
    conda "./envs/homer.yaml"

    cpus "8"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(sample_id), file(peaks), file(background) from peaks_with_background_homer_ch
    each file(genome_fasta) from reference_for_homer_bg_ch

    output:
    file "${out_dir}/*"

    script:
    out_dir = "homer_bg/${sample_id}"
    """
    mkdir -vp "./${out_dir}"
    findMotifsGenome.pl \
        ${peaks} \
        ${genome_fasta} \
        ${out_dir} \
        -bg ${background} \
        -h \
        -size given \
        -len ${params.homer_len} \
        -p 8 \
        -S 25 \
        -gc \
        2>&1 \
        | tee ${out_dir}/${sample_id}.log
    """
}

id_bam_for_summary_ch
    .multiMap { it ->
        id: it[0]
        bam: it[1]
    }
    .set {
        summary_ch
    }

process multibamsummary {
    conda "./envs/deeptools.yaml"

    cpus "48"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    val(ids) from summary_ch.id.collect()
    file(bams) from summary_ch.bam.collect()
    file(index) from bam_index_ch.collect()

    output:
    file("plotpca/multibamsummary.npz") into coverage_matrix_ch
    file("plotpca/multibamsummary.log")

    """
    mkdir -v ./plotpca
    { multiBamSummary bins \
        --verbose \
        --numberOfProcessors ${task.cpus} \
        --binSize ${params.multibamsummary_binsize} \
        --labels ${ids} \
        --bamfiles ${bams} \
        --outFileName ./plotpca/multibamsummary.npz; } \
    &>./plotpca/multibamsummary.log
    """
}

process plotpca {
    conda "./envs/deeptools.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    file(matrix) from coverage_matrix_ch

    output:
    file("plotpca/pca.tab") into pca_ch
    file("plotpca/pca.pdf")
    file("plotpca/plotpca.log")

    """
    mkdir -v ./plotpca
    { plotPCA \
        --transpose \
        --corData ${matrix} \
        --plotFile ./plotpca/pca.pdf \
        --plotTitle "${params.plotpca_title}" \
        --outFileNameData ./plotpca/pca.tab; } \
    &>./plotpca/plotpca.log
    """
}

process makewindows {
    conda "./envs/bedtools.yaml"

    cpus "1"
    memory "10GB"

    storeDir params.reference_dir

    input:
    file(genome) from genome_for_windows_ch

    output:
    file("${genome.getBaseName()}.${params.bedtools_window_size}.windows") into windows_ch

    """
    bedtools makewindows \
        -g ${genome} \
        -w ${params.bedtools_window_size} \
        >${genome.getBaseName()}.${params.bedtools_window_size}.windows
    """
}

process coverage {
    conda "./envs/bedtools.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    each file(windows) from windows_ch
    tuple val(sample_id), file(bam) from bam_for_coverage_ch

    output:
    file("bedtools/${sample_id}.coverage.bed") into coverage_ch

    """
    mkdir -v ./bedtools
    bedtools coverage \
        -split \
        -sorted \
        -counts \
        -F 0.5 \
        -a ${windows} \
        -b ${bam} \
        >"./bedtools/${sample_id}.coverage.bed"
    """
}

process plotpca_r {
    conda "./envs/ggplot2.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    file(beds) from coverage_ch.collect()

    output:
    file("R/*_mqc.png") into pca_r_ch

    """
    mkdir -v ./R
    plotpca.R ${beds}
    cp -v *_mqc.png ./R/
    """
}

process multiqc {
    conda "./envs/multiqc.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    file "cutadapt/*" from cutadapt_log_ch.collect()
    file "fastqc/*" from fastqc_ch.collect()
    file "samtools/*" from samtools_stats_ch2.collect()
    file "picard/*" from mark_dup_ch.collect()
    file "count_table_mqc.txt" from count_table_ch
    file "preseq/*" from preseq_ch.collect()
    file "preseq/*" from preseq_all_counts_ch
    file "macs2/*" from macs2_qc_ch.collect()
    file "deeptools/*" from fragment_qc_ch.collect().ifEmpty { 'NO_FRAGMENT_QC' }
    file "deeptools/*" from fingerprint_qc_ch.collect()
    file "deeptools/*" from profile_qc_ch.collect()
    file "deeptools/pca.tab" from pca_ch
    file "R/*_mqc.png" from pca_r_ch

    output:
    file "multiqc/multiqc_report.html"
    file "multiqc/multiqc_data"
    file "multiqc/multiqc.log"

    """
    for f in deeptools/*.profile.tsv
    do
        sed -i 's/${params.deeptools_label}\t/TSS\t/' \$f
    done
    mkdir -v "./multiqc"
    { multiqc -o "./multiqc" .; } >./multiqc/multiqc.log
    """
}
